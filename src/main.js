import Vue from 'vue'
import VueMaterial from 'vue-material'
import VueRouter from 'vue-router'
import { routes } from './route.js'
import 'vue-material/dist/vue-material.css'
import 'vue-material/dist/theme/black-green-light.css'
import 'material-icons/iconfont/material-icons.scss'

import App from './app.vue'

Vue.config.productionTip = false
Vue.use(VueMaterial)
Vue.use(VueRouter)

const router = new VueRouter({ routes })

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
